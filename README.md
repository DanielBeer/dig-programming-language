# Welcome to D!g.

D!g, pronounced "dig", is a stack-oriented esoteric programming language designed in 2019 by Daniel Beer. It consists of series of commands within closures to perform input and output operations, and manipulation of the stack.

A fully-functional interpreter for D!g, written in JavaScript, is available via [GitLab Pages](https://danielbeer.gitlab.io/dig-programming-language/).

## Specifications

### Typing

All values in D!g are signed integers.

### Closures

Statements in D!g are made up of a series of valid commands enclosed in a closure. Closures can be nested. Every statement of a program must be within a closure, and closures may contain other closures. A program terminates when it reaches the end of a closure.

### Commands

#### Input and output

- `in` : Read an (unsigned) integer from standard input
- `cin` : Read a character from standard input as an (unsigned) integer
- `out` : Pop from the stack and write the popped value to standard output
- `cout` : Pop from the stack and write the popped value as a character to standard output

**Example** (identity):

```
[in out]
```

#### Arithmetic operations

- `+` : Add the following two values and push the result to the stack
- `-` : Subtraction
- `*` : Multiplication
- `/` : Pushes the remainder of the integer division of the operands and the quotient (in that order) to the stack

*Note: There are no logical or relational operators in D!g.*

#### Stack manipulation

- `.` : Pops from the stack and is interpreted as the popped value
- `$` : Pops the value on top of the stack and pushes it back twice (duplication)
- `!` : Swaps the order of the top two values on the stack
- `#` : Pushes the depth of the closure as an (unsigned) integer to the stack (outermost closure has depth 1)

**Example** (successor):

```
[# in +.. out]
```

**Example** (negation):

```
[in # $ -.. -.. out]
```

**Example** (modulo):

```
[in in ! /.. . out]
```

#### Closure jumping

- `>` : Jumps to the beginning of the next nested closure
- `<` : Jumps to the beginning of the closure which encloses the current closure

Closure jumping operators can be repeated. A sequence of the same consecutive operators will perform that many jumps. (i.e. `>>>` will jump three deeper)

**Example** (hello world!):

```
[# $ $ +.. $ +.. $ +.. ! $ $ +.. +.. $ $ +.. ! # $
 [# ! # $
  [# $ *..
   [! # *.. *.. cout
    [# *.. $ *.. +.. $ cout >>
     [# >>>>
      [# +.. $ cout $ cout +.. +.. cout
       [# $ +.. $ +.. cout
        [# $ *.. <<<
         [+.. cout # $ *..
          [# +.. $ cout +.. $ cout -.. $ cout -.. cout # $ $ +.. +.. cout]]]]]]]]]]]
```

#### Conditional operating

- `?` : Pops from the stack; if the popped value is zero (false) the following operator is skipped, otherwise it has no effect other than popping the top value of the stack. (i.e. `?<<` jump out two if the top value of the stack is nonzero)

**Example** (logical and):
```
[# $ $ -.. in in ?>
                  . >>>

 [?>
   >>
   
  [.
   [out]]]]
```

**Example** (reverse a string):

```
[# $ -.. in cin .
 [$ ?>
     >>>

  [cin ! #
   [# -.. ! -.. <<
    [.
     [$ ?>
         >>

      [cout <
       []]]]]]]]
```

### Sequencing

Throughout execution, a pointer is maintained to the top of the stack. Outside of a sequence (or at the end of a sequence), the stack pointer returns to the top of the full stack. Sequencing allows for multiple operations to be performed before the stack pointer resets. When inside a sequence, the stack pointer will not move to compensate for values pushed to the stack. A sequence is a statement wrapped in curly braces and evaluates to the value of the last command in the enclosed statement. Sequences cannot contain closures.

**Example** (factorial):
```
[# in $ ?>
         # >>>>>
 [{$ $} >>
  [{! $} !
   [{$ -..} {! $} {! ?>
                      >>}
    [{! $} . *.. <<
     [out]]]]]]
```
