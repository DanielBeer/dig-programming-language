const jumpable = Object.freeze(["NullOp", "BinOp", "CondExec", "ClosureJump"]);

// ******************

//       ENUMS

// ******************

export const OpEnum = Object.freeze({
	"null" : ['.', '$', '!', '#'],
	"nullIO" : ["in", "out", "cin", "cout"],
	"bin" : ['+', '-', '*', '/']
});
export const ClosureCharEnum = Object.freeze({'[' : ']', '{' : '}'});
export const DirectionEnum = Object.freeze({"outwards" : -1, "inwards" : 1});

// ******************

//       TOKENS

// ******************

class Closure {
	constructor(stmt, outer, depth) {
		this.outer = outer;
		this.depth = depth + 1;
		this.stmt = parse(stmt, this, this.depth);
	}
}

class Seq {
	constructor(stmt, cur, depth) { this.stmt = parse(stmt, cur, depth); }
}

class NullOp {
	constructor(op, producer) { this.op = op; this.producer = producer }
}

class BinOp {
	constructor(op, x, y) { this.op = op, this.x = x, this.y = y; }
}

class CondExec {
	constructor(x) { this.x = x; }
}

class ClosureJump {
	constructor(n, dir) { this.n = n; this.dir = dir; }
}

// ******************

//       PARSER

// ******************

export var parseFailed;
var lineNum;

export function refreshParseVars() {
	parseFailed = false;
	lineNum = 1;
}

function parseError(errorName, found, expected) {
	parseFailed = true;
	
	var varString = "";
	
	if (found) {
		if (expected) {
			varString = ". Expected: " + expected +
				", found: " + found + "\n";
		} else varString = ". Found: " + found + "\n";
	}
	
	document.getElementById("stdout").value =
		"Parse error: " + errorName +
		" at line " + lineNum.toString() +
		varString + "\n";
}

function checkSeqOutput(seq) {
	if (seq.stmt.length) {
		let last = seq.stmt[seq.stmt.length - 1];
		
		if (last.producer) return true;
		else if (last.constructor.name === "Seq") return checkSeqOutput(last.stmt);
		else return false;
	}
	
	return false;
}

function findClosure(str, open, close) {
	var count = 1;
	
	for (let i=1; i<str.length; ++i) {
		const c = str.charAt(i);
		
		if (c === open) count += 1;
		else if (c === close) count -= 1;
		
		if (count === 0) {
			return i;
		}
	}
	
	if (open === '[') parseError("Incomplete closure");
	else if (open === '{') parseError("Incomplete sequence");
	
	return;
}

export function parse(str, curClosure, depth) {
	var ptr = 0;
	var cmds = [];
		
	if (curClosure === null && !ClosureCharEnum[str.trimLeft().charAt(0)]) {
		parseError("Commands not in closure"); 
		
		return;
	}
	
	while (ptr < str.length && !parseFailed) {
		const c = str.charAt(ptr);
		
		if (ClosureCharEnum[c]) {
			const end = findClosure(str.slice(ptr), c, ClosureCharEnum[c]) + ptr;
			const inner = str.slice(ptr + 1, end);
			
			if (c === '[') cmds.push(new Closure(inner, curClosure, depth));
			else if (c === '{') cmds.push(new Seq(inner, curClosure, depth));
			
			ptr = end + 1;
		} else if (OpEnum.null.includes(c)) {
			cmds.push(new NullOp(c, c === '.' || c === '#'));
			ptr += 1;
		} else if (OpEnum.bin.includes(c)) {
			const rest = parse(str.slice(ptr + 1), curClosure, depth);
			
			let first = rest[0], second = rest[1];
			
			if (first == null || second == null) {
				parseError("Expected two args. for binary operator", "end of statement");
				
				return;
			} else if ((first.constructor.name === "Seq" && !checkSeqOutput(first)) || (second.constructor.name === "Seq" && !checkSeqOutput(second))) {
				parseError("Expected two args. for binary operator", "Sequence producing null");
				
				return;
			} else if (first.constructor.name === "Closure" || second.constructor.name === "Closure") {
				parseError("Expected two args. for binary operator", "Closure");
				
				return;
			} else if ((first.constructor.name !== "Seq" && !first.producer) || (second.constructor.name !== "Seq" && !second.producer)) {
				parseError("Expected two args. for binary operator", "operator producing null");
				
				return;
			}
			
			cmds.push(new BinOp(c, first, second));
			cmds = cmds.concat(rest.slice(2));
			
			break;
		} else if (c === '?') {
			const rest = parse(str.slice(ptr + 1), curClosure, depth);
			
			if (!rest) return;
			
			let arg = rest[0];
			
			if (!arg) {
				if (!parseFailed) parseError("Expected arg. for conditional", "end of statement");
				
				return;
			}
			
			if (jumpable.includes(arg.constructor.name)) {
				cmds.push(new CondExec(rest[0]));
				cmds = cmds.concat(rest.slice(1));
				
				break;
			} else {
				parseError("Invalid conditional", arg.constructor.name, "operator");
				
				return;
			}
		} else if (/[a-z]/i.test(c)) {
			const cmd = str.slice(ptr).match(/[a-z]+/i)[0];
			
			if (!OpEnum.nullIO.includes(cmd)) parseError("Invalid token", cmd);
			
			cmds.push(new NullOp(cmd));
			ptr += cmd.length;
		} else if (c === '>') {
			const cmd = str.slice(ptr).match(/>+/i)[0];
			
			cmds.push(new ClosureJump(cmd.length, DirectionEnum.inwards));
			ptr += cmd.length;
		} else if (c === '<') {
			const cmd = str.slice(ptr).match(/<+/i)[0];
			
			cmds.push(new ClosureJump(cmd.length, DirectionEnum.outwards));
			ptr += cmd.length;
		} else if (/\s+/.test(c)) {
			if (c === '\n') lineNum += 1;
			
			ptr += 1;
		} else parseError("Invalid token", c);
	}
	
	return cmds;
}