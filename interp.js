import {DirectionEnum} from "./parse.js";

var memStack = [], curClosure, input, halted, sequencing, sp;

export function refreshInterpVars() {
	memStack = [];
	curClosure = null;
	input = document.getElementById("stdin").value;
	halted = false;
	sequencing = false;
	sp = 0;
}

function interpError(errorName) {
	sequencing = false;
	halted = true;
	
	document.getElementById("stdout").value =
		"Interpreter error: " + errorName +
		" from depth " + curClosure.depth.toString() + "\n";
}		

function memPush(v) {
	memStack.splice(sp, 0, v)
	
	if (sequencing) sp += 1;
};

function memPop() {
	let v = memStack[sp];

	if (v == null) interpError("Tried to pop and saw empty stack at SP = " + sp.toString());

	memStack.splice(sp, 1);
	
	return v;
}

function readInt() {
	input = input.trimLeft();
	
	if (input.length === 0) {
		interpError("Tried to read int but found no inputs");
	
		return;
	}
	
	var val = 0;
	var c = input.charAt(0);
	
	while (input.length > 0 && !/\s/.test(c)) {
		val = 10 * val + parseInt(c);
		input = input.slice(1);
		
		c = input.charAt(0);
	}
	
	return val;
}

function readChar() {
	if (input.length === 0) {
		interpError("Tried to read char but found no inputs");
	
		return;
	}
	
	const i = input.charCodeAt(0);
	
	input = input.slice(1);
	
	return i;
}

function nullOpCall(op) {
	switch (op) {
		case '.':
			return memPop();
		case '$':
			const top = memPop();
			
			memPush(top);
			return memPush(top);
		case '!':
			const first = memPop();
			const second = memPop();
			
			memPush(first);
			
			if (sequencing) sp -= 1;
			
			memPush(second);
			
			if (sequencing) sp += 1;
			
			return;
		case '#':
			return memPush(curClosure.depth);
		case "in":
			return memPush(readInt());
		case "out":
			return document.getElementById("stdout").value += memPop().toString();
		case "cin":
			return memPush(readChar());
		case "cout":
			return document.getElementById("stdout").value += String.fromCharCode(memPop());
	}
}

function binOpCall(op, x, y) {
	const xRes = interpCmd(x), yRes = interpCmd(y);
	
	switch (op) {
		case '+':
			return memPush(xRes + yRes);
		case '-':
			return memPush(xRes - yRes);
		case '*':
			return memPush(xRes * yRes);
		case '/':
			memPush(xRes % yRes);
			return memPush(Math.floor(xRes / yRes));
	}
}

function condExecCall(x) {
	var popped = memPop();
	
	if (popped !== 0) {
		interpCmd(x);
	}
}

function closureJumpCall(n, dir) {
	sequencing = false;
	sp = 0;
	
	var nextClosure = curClosure;
	
	if (dir == DirectionEnum.outwards) {
		for (let i=0; i<n; ++i) {
			if (nextClosure == null) {
				interpError("Jump of " + n.toString() + " exceeded outermost closure");
				
				return;
			}
			
			nextClosure = nextClosure.outer;
		}
		
		if (nextClosure == null) interpError("Jump of " + n.toString() + " exceeded outermost closure");
	} else {
		let found;
		
		for (let i=0; i<n; ++i) {
			found = false;
			
			nextClosure.stmt.forEach(cmd => {
				if (cmd.constructor.name === "Closure") {
					nextClosure = cmd;
					found = true;
					
					return;
				}
			});
		}
		
		if (!found) interpError("Jump of " + n.toString() + " cannot reach deeper closure");
	}
	
	if (halted) return;
	
	curClosure = nextClosure;
	
	interp(curClosure.stmt);
}

function interpSeq(cmds) {
	sequencing = true;

	var res;

	cmds.forEach(cmd => {
		if (!sequencing || halted) return res;
		
		res = interpCmd(cmd);
	});

	sequencing = false;
	sp = 0;
	
	return res;
}

function interpCmd(cmd) {	
	if (halted) return;

	switch (cmd.constructor.name) {
		case "Closure":
			curClosure = cmd;
			
			return interp(curClosure.stmt);
		case "Seq":
			return interpSeq(cmd.stmt);
		case "NullOp":
			return nullOpCall(cmd.op);
		case "BinOp":
			return binOpCall(cmd.op, cmd.x, cmd.y);
		case "CondExec":
			return condExecCall(cmd.x);
		case "ClosureJump":
			return closureJumpCall(cmd.n, cmd.dir);
	}
}

export function interp(cmds) {
	cmds.forEach(cmd => {
		if (halted) return;
		
		interpCmd(cmd);
	});

	halted = true;
}