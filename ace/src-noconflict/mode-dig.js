ace.define("ace/mode/dig_highlight_rules",["require","exports","module","ace/lib/oop","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var DigHighlightRules = function() {
	this.$rules = {
		"start" : [
			{
				token : "keyword",
				regex : "\\b(c?out|c?in)\\b",
				caseInsensitive : false
			},
			{
				token : "keyword.control",
				regex : "\\?"
			},
			{
				token : "keyword.control",
				regex : "[><]+"
			},
			{
				token : "support.function",
				regex : "[.$!#]",
			},
			{
				token : "keyword.operator",
				regex : "[*+-/]"
			}
		]
	};
	
	this.normalizeRules();
};

DigHighlightRules.metaData = { fileTypes: [ 'dg' ],
	name: 'D!g',
	scopeName: 'source.dig' };


oop.inherits(DigHighlightRules, TextHighlightRules);

exports.DigHighlightRules = DigHighlightRules;
});

ace.define("ace/mode/dig",["require","exports","module","ace/lib/oop","ace/mode/text","ace/mode/text_highlight_rules","ace/mode/behaviour"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var TextMode = require("./text").Mode;
var DigHighlightRules = require("./dig_highlight_rules").DigHighlightRules;
var Behaviour = require("./behaviour").Behaviour;

var Mode = function() {
    this.HighlightRules = DigHighlightRules;
    this.$behaviour = new Behaviour();
};

oop.inherits(Mode, TextMode);

(function() {
    this.$id = "ace/mode/dig";
}).call(Mode.prototype);

exports.Mode = Mode;
});                (function() {
                    ace.require(["ace/mode/dig"], function(m) {
                        if (typeof module == "object" && typeof exports == "object" && module) {
                            module.exports = m;
                        }
                    });
                })();